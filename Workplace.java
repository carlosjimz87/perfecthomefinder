package modules;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Workplace {

    @SerializedName("x")
    @Expose
    public Integer x;
    @SerializedName("y")
    @Expose
    public Integer y;

    public Integer getX() {
        return x;
    }

    public Integer getY() {
        return y;
    }
}
