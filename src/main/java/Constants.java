public class Constants {

    //STRINGS
    public static final String INPUT_FILENAME = "input.json";
    public static final String CORRUPT_INPUT_FILENAME = "corrupt_input.json";
    public static final int MAX_SCORE = 100;

    //MESSAGES

    public static final String PERFECT_HOME_STRING = "The perfect house for %s is the %s";
    public static final String ERROR_NO_INPUT_FILE = "No input file!";
    public static final String ERROR_INPUT_FILE_NOT_FOUND = "Input file not found!";
    public static final String ERROR_ERROR_READING_JSON = "Invalid json!";
    public static final String ERROR_ERROR_CLOSING_READ_BUFFER = "Error closing read buffer!";
    public static final String ERROR_INVALID_ARGUMENTS = "Invalid or non-existent arguments!";
    public static final String NO_PERFECT_HOMES = "No perfect home was found!";

}
