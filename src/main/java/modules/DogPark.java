package modules;

public class DogPark extends Location {


    public DogPark(Integer x, Integer y){
        setX(x);
        setY(y);
    }

    public Integer pointsPerHomeAndFamily(Home home, Family family) {
        return distanceToPoint(home.getX(),home.getY())*family.getDogs();
    }
}