package modules;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Family {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("workers")
    @Expose
    private Integer workers;
    @SerializedName("children")
    @Expose
    private Integer children;
    @SerializedName("dogs")
    @Expose
    private Integer dogs;
    @SerializedName("Budget")
    @Expose
    private Integer budget;

    public Family(String name, Integer workers, Integer children, Integer dogs, Integer budget) {
        setName(name);
        setWorkers(workers);
        setChildren(children);
        setDogs(dogs);
        setBudget(budget);
    }

    public String getName() {
        return name;
    }

    public Integer getWorkers() {
        return workers;
    }

    public Integer getChildren() {
        return children;
    }

    public Integer getDogs() {
        return dogs;
    }

    public Integer getBudget() {
        return budget;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setWorkers(Integer workers) {
        this.workers = workers;
    }

    public void setChildren(Integer children) {
        this.children = children;
    }

    public void setDogs(Integer dogs) {
        this.dogs = dogs;
    }

    public void setBudget(Integer budget) {
        this.budget = budget;
    }



}