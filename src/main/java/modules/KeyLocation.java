package modules;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class KeyLocation {

    @SerializedName("pools")
    @Expose
    private List<Pool> pools;
    @SerializedName("workplaces")
    @Expose
    private List<Workplace> workplaces ;
    @SerializedName("dogParks")
    @Expose
    private List<DogPark> dogParks ;

    public KeyLocation(List<Pool> pools, List<Workplace> workplaces, List<DogPark> dogParks) {
        this.pools = pools;
        this.workplaces = workplaces;
        this.dogParks = dogParks;
    }

    public void setPools(List<Pool> pools) {
        this.pools = pools;
    }

    public void setWorkplaces(List<Workplace> workplaces) {
        this.workplaces = workplaces;
    }

    public void setDogParks(List<DogPark> dogParks) {
        this.dogParks = dogParks;
    }

    public List<Pool> getPools() {
        return pools;
    }

    public List<Workplace> getWorkplaces() {
        return workplaces;
    }

    public List<DogPark> getDogParks() {
        return dogParks;
    }
}
