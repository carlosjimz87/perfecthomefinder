package modules;

public class Workplace extends Location{


    public Workplace(Integer x, Integer y){
        setX(x);
        setY(y);
    }

    public Integer pointsPerHomeAndFamily(Home home, Family family) {
        return distanceToPoint(home.getX(),home.getY())*family.getWorkers();
    }

}
