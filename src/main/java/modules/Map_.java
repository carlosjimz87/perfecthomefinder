package modules;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Map_ {

    @SerializedName("height")
    @Expose
    private Integer height;
    @SerializedName("width")
    @Expose
    private Integer width;

    public Map_(int h, int w){ height=h; width=w; }

    public Integer getHeight() {
        return height;
    }

    public Integer getWidth() {
        return width;
    }


}
