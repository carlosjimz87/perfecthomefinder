package modules;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Map {

    @SerializedName("map")
    @Expose
    private Map_ map;
    @SerializedName("keyLocations")
    @Expose
    private KeyLocation keyLocations;
    @SerializedName("homes")
    @Expose
    private List<Home> homes = null;
    @SerializedName("families")
    @Expose
    private List<Family> families = null;

    public Map(Map_ map, KeyLocation keyLocations, List<Home> homes, List<Family> families) {
        setFamilies(families);
        setHomes(homes);
        setKeyLocations(keyLocations);
        setMap(map);
    }

    public Map_ getMap() {
        return map;
    }


    public KeyLocation getKeyLocations() {
        return keyLocations;
    }


    public List<Home> getHomes() {
        return homes;
    }


    public List<Family> getFamilies() {
        return families;
    }

    public void setMap(Map_ map) {
        this.map = map;
    }

    public void setKeyLocations(KeyLocation keyLocations) {
        this.keyLocations = keyLocations;
    }

    public void setHomes(List<Home> homes) {
        this.homes = homes;
    }

    public void setFamilies(List<Family> families) {
        this.families = families;
    }


}


