package modules;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

interface _Distanceable{
     Integer distanceToPoint(Integer x, Integer y);
}


public abstract class Location implements _Distanceable{

     @SerializedName("x")
     @Expose
     private Integer x;
     @SerializedName("y")
     @Expose
     private Integer y;


     public Integer getX() {
          return x;
     }

     public Integer getY() {
          return y;
     }

     public void setX(Integer x) {
          this.x = x;
     }

     public void setY(Integer y) {
          this.y = y;
     }

     public Integer distanceToPoint(Integer x, Integer y) {
          return Math.abs(this.x - x)+Math.abs(this.y-y);
     }

     public abstract Integer pointsPerHomeAndFamily(Home home, Family family) ;

}



