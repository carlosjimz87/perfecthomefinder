package modules;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Home  extends Location{

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("price")
    @Expose
    private Integer price;

    public Home(String n, Integer x, Integer y, Integer p){
       setX(x);
       setY(y);
       setName(n);
       setPrice(p);
    }

    public String getName() {
        return name;
    }

    public Integer getPrice() {
        return price;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }


    public Integer pointsPerHomeAndFamily(Home home, Family family) {
        return 0;
    }
}

