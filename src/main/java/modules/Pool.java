package modules;

public class Pool extends Location{


    public Pool(Integer x, Integer y){
        setX(x);
        setY(y);
    }

    public Integer pointsPerHomeAndFamily(Home home, Family family) {
        return distanceToPoint(home.getX(),home.getY()) * family.getChildren();
    }


}