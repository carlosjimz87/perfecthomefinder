import modules.Map;

import java.io.File;
import java.util.List;

public class Main {

    public static void main(String[] args){

        File file = PerfectHomeFinder.readFileFromArgs((args));

        if (file!=null) {

            Map map = PerfectHomeFinder.parseInputFile(file);

            if(PerfectHomeFinder.isValidMap(map)){
                    List<String> perfectHomes = PerfectHomeFinder.findPerfectHomes(map);

                    PerfectHomeFinder.printPerfectHomes(perfectHomes);
            }
        }
    }
}
