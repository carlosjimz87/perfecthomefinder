import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import modules.*;

import java.io.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class PerfectHomeFinder {
    static Map_ mapzone;


    public static boolean isValidMap(Map map){
        if(map==null) return false;
        if(map.getKeyLocations()==null) return false;
        if(map.getHomes()==null) return false;
        if(map.getFamilies()==null) return false;
        if(map.getKeyLocations().getPools()==null) return false;
        if(map.getKeyLocations().getWorkplaces()==null) return false;
        return map.getKeyLocations().getDogParks() != null;

    }


    public static int printPerfectHomes(List<String> perfectHomes){


        if(perfectHomes==null || perfectHomes.size()==0){
            System.out.println(Constants.NO_PERFECT_HOMES);
            return 0;
        }

            for(String pf: perfectHomes){
                System.out.println(pf);

            }
        return 1;
    }

    public static List<String> findPerfectHomes(Map map) {
        List<String> perfectHomes = new ArrayList<>();
        mapzone = map.getMap();


        for (Family f : map.getFamilies()) {
            Home selectedHome = null;
            Integer highestScore = 0;

            for (Home h : map.getHomes()) {
                Integer newScore = getHomePoints(h,f, map.getKeyLocations());

                if(newScore > highestScore && h.getPrice() <= f.getBudget()){
                    selectedHome = h;
                    highestScore = newScore;
                }

            }
            if(selectedHome!=null)
                perfectHomes.add(String.format(Constants.PERFECT_HOME_STRING,f.getName(),selectedHome.getName()));

        }
        return perfectHomes;
    }


    public static int getHomePoints( Home home, Family family, KeyLocation locations){

        return Constants.MAX_SCORE
                - shortestDistance(getDistances(locations.getPools(),home))*family.getChildren()
                - shortestDistance(getDistances(locations.getDogParks(),home))*family.getDogs()
                - shortestDistance(getDistances(locations.getWorkplaces(),home))*family.getWorkers()
                ;
    }

    public static <T> List<Integer> getDistances(List<T> locations, Home home){
        List<Integer> locationDistances = new ArrayList<>();
        for(T d:locations){
            // checking that every point is inside the map
                locationDistances.add(((Location)d).distanceToPoint(home.getX(),home.getY()));
        }
        return locationDistances;

    }

    public static Integer shortestDistance(List<Integer> distances){
        if(distances!= null && !distances.isEmpty())
        return distances.stream().min(Comparator.naturalOrder()).get();
        else return null;
    }

    public static boolean insideMapZone(int x, int y, Map_ mapzone){
        return (x < mapzone.getWidth() && y < mapzone.getHeight());
    }

    /**  REGION :: INPUT FILE  ::  **/
    public static Map parseInputFile(File file){

        if(file==null) {
            System.err.println(Constants.ERROR_NO_INPUT_FILE);
            return null;
        }
        Map map = null;
        Gson gson = new Gson();
        BufferedReader buffer = null;
        try {
            buffer = new BufferedReader(new FileReader(file.getPath()));
            map = gson.fromJson(buffer, Map.class);

        } catch (FileNotFoundException e) {
            System.err.println(Constants.ERROR_INPUT_FILE_NOT_FOUND);
        } catch (JsonParseException e) {
           // e.printStackTrace();
            System.err.println(Constants.ERROR_ERROR_READING_JSON);
        }
        finally {
            if (buffer != null) {
                try {
                    buffer.close();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    System.err.println(Constants.ERROR_ERROR_CLOSING_READ_BUFFER);
                }
            }
        }
    return map;
    }


    public static File readFileFromArgs(String[] args){

        if(args!=null && 0 < args.length){
            File file = new File(args[0]);
            return (file.exists() && file.canRead())? file:null;
        }
        else{
            System.err.println(Constants.ERROR_INVALID_ARGUMENTS);
            return null;
        }
    }


    /** ENDREGION  **/


}
