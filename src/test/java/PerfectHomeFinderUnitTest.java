import modules.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import java.io.File;
import java.util.ArrayList;
import java.util.List;


public class PerfectHomeFinderUnitTest {

    private Map map;
    private List<String> perfectHomesStrings = new ArrayList<String>(){};

    @Before
    public void setUp() {
        perfectHomesStrings.add(String.format(Constants.PERFECT_HOME_STRING,"The Simpsons","Comfy condo"));
        perfectHomesStrings.add(String.format(Constants.PERFECT_HOME_STRING,"The Van Houtens","Lofty loft"));

        Map_ mapzone = new Map_(5,5);

        ArrayList<Home> homes = new ArrayList<Home>();

        homes.add(new Home("Comfy condo",1,2,250000));
        homes.add(new Home("Lofty loft",4,4,185000));
        homes.add(new Home("Burns Manor",1,3,9999999));

        ArrayList<Family> families = new ArrayList<Family>();

        families.add(new Family("The Simpsons",1,3,1,255000));
        families.add(new Family("The Van Houtens",2,1,1,195000));

        ArrayList<Workplace> workplaces = new ArrayList<Workplace>();

        workplaces.add(new Workplace(3,4));
        workplaces.add(new Workplace(1,1));


        ArrayList<Pool> pools = new ArrayList<Pool>();

        pools.add(new Pool(0,1));
        pools.add(new Pool(2,3));


        ArrayList<DogPark> dogparks = new ArrayList<DogPark>();

        dogparks.add(new DogPark(2,2));


        KeyLocation keyLocation = new KeyLocation(pools,workplaces,dogparks);
        map  = new Map(mapzone,keyLocation,homes,families);
    }

    @Test
    public void printPerfectHomesSuccessful(){
        Assert.assertEquals(1, PerfectHomeFinder.printPerfectHomes(perfectHomesStrings));
    }

    @Test
    public void printPerfectHomesNull(){
        Assert.assertEquals(0, PerfectHomeFinder.printPerfectHomes(null));
    }

    @Test
    public void printPerfectHomesEmpty(){
        Assert.assertEquals(0, PerfectHomeFinder.printPerfectHomes(new ArrayList<>()));
    }

    @Test
    public void isValidMap(){
        Assert.assertTrue(PerfectHomeFinder.isValidMap(map));
    }


    @Test
    public void findPerfectHomes(){

        Assert.assertArrayEquals(perfectHomesStrings.toArray(), PerfectHomeFinder.findPerfectHomes(map).toArray());
    }

    @Test
    public void getHomePointsForHomeAndFamily(){

        // FAMILY A - HOME A
        Home home = map.getHomes().get(0);
        Family family = map.getFamilies().get(0);
        Assert.assertEquals(92,PerfectHomeFinder.getHomePoints(home,family,map.getKeyLocations()));


        // FAMILY A - HOME B
        home = map.getHomes().get(1);
        family = map.getFamilies().get(0);
        Assert.assertEquals(86,PerfectHomeFinder.getHomePoints(home,family,map.getKeyLocations()));


        // FAMILY A - HOME C
        home = map.getHomes().get(2);
        family = map.getFamilies().get(0);
        Assert.assertEquals(93,PerfectHomeFinder.getHomePoints(home,family,map.getKeyLocations()));
    }

    @Test
    public void getDistancesFromPool() {
        Home home = map.getHomes().get(0);
        Assert.assertArrayEquals(new Integer[]{2,2},PerfectHomeFinder.getDistances(map.getKeyLocations().getPools(),home).toArray());
    }

    @Test
    public void getDistancesFromWorkplace() {
        Home home = map.getHomes().get(0);
        Assert.assertArrayEquals(new Integer[]{4,1},PerfectHomeFinder.getDistances(map.getKeyLocations().getWorkplaces(),home).toArray());
    }

    @Test
    public void getDistancesFromDogPark() {
        Home home = map.getHomes().get(0);
        Assert.assertArrayEquals(new Integer[]{1},PerfectHomeFinder.getDistances(map.getKeyLocations().getDogParks(),home).toArray());
    }

    @Test
    public void shortestDistanceWithNull() {
        Assert.assertNull(PerfectHomeFinder.shortestDistance(null));
    }

    @Test
    public void shortestDistanceWithEmpty() {
        Assert.assertNull(PerfectHomeFinder.shortestDistance(new ArrayList<Integer>(){}));
    }

    @Test
    public void shortestDistanceCorrect() {
        List<Integer> list = new ArrayList<Integer>(){
            {
                add(1);
                add(2);
                add(3);
                add(4);
                add(5);
            }
        };
        Assert.assertEquals(1,(long)PerfectHomeFinder.shortestDistance(list));
    }


    @Test
    public void insideMapZoneInside() {
      //  Assert.assertTrue(PerfectHomeFinder.insideMapZone(1,1,new Map_(5,5)));
    }

    @Test
    public void insideMapZoneOutside() {
      //  Assert.assertFalse(PerfectHomeFinder.insideMapZone(6,1,new Map_(5,5)));
    }

    @Test
    public void readFileFromArgsNull() {
        Assert.assertNull(PerfectHomeFinder.readFileFromArgs(null));
    }

    @Test
    public void readFileFromArgsEmpty() {
        Assert.assertNull(PerfectHomeFinder.readFileFromArgs(new String[]{}));
    }

    @Test
    public void readFileFromArgsWithNonInputText() {
        Assert.assertNull(PerfectHomeFinder.readFileFromArgs(new String[]{"NonFile"}));
    }

    @Test
    public void readFileFromArgsWithInputFile() {
        Assert.assertEquals(new File(Constants.INPUT_FILENAME),PerfectHomeFinder.readFileFromArgs(new String[]{Constants.INPUT_FILENAME}));
    }

    @Test
    public void parseInputWithNull() {
        Assert.assertNull(PerfectHomeFinder.parseInputFile(null));
    }

    @Test
    public void parseInputWithCorruptInputFile() {
        File file = PerfectHomeFinder.readFileFromArgs(new String[]{Constants.CORRUPT_INPUT_FILENAME});
        Assert.assertNull(PerfectHomeFinder.parseInputFile(file));
    }

    @Test
    public void parseInputWithInputFile() {

        File file = PerfectHomeFinder.readFileFromArgs(new String[]{Constants.INPUT_FILENAME});
        Map rmap =PerfectHomeFinder.parseInputFile(file);
        Assert.assertNotNull(rmap);

        //COMPARING MAPZONE
        Assert.assertEquals(map.getMap().getHeight(),rmap.getMap().getHeight());
        Assert.assertEquals(map.getMap().getWidth(),rmap.getMap().getWidth());

        //COMPARING HOMES
        Assert.assertEquals(map.getHomes().size(),rmap.getHomes().size());

        Assert.assertEquals(map.getHomes().get(0).getName(),rmap.getHomes().get(0).getName());
        Assert.assertEquals(map.getHomes().get(0).getPrice(),rmap.getHomes().get(0).getPrice());
        Assert.assertEquals(map.getHomes().get(0).getX(),rmap.getHomes().get(0).getX());
        Assert.assertEquals(map.getHomes().get(0).getY(),rmap.getHomes().get(0).getY());

        //COMPARING FAMILIES
        Assert.assertEquals(map.getFamilies().size(),rmap.getFamilies().size());

        Assert.assertEquals(map.getFamilies().get(0).getName(),rmap.getFamilies().get(0).getName());
        Assert.assertEquals(map.getFamilies().get(0).getChildren(),rmap.getFamilies().get(0).getChildren());
        Assert.assertEquals(map.getFamilies().get(0).getDogs(),rmap.getFamilies().get(0).getDogs());
        Assert.assertEquals(map.getFamilies().get(0).getWorkers(),rmap.getFamilies().get(0).getWorkers());
        Assert.assertEquals(map.getFamilies().get(0).getBudget(),rmap.getFamilies().get(0).getBudget());

        //COMPARING POOLS
        Assert.assertEquals(map.getKeyLocations().getPools().size(),rmap.getKeyLocations().getPools().size());

        Assert.assertEquals(map.getKeyLocations().getPools().get(0).getX(),rmap.getKeyLocations().getPools().get(0).getX());
        Assert.assertEquals(map.getKeyLocations().getPools().get(0).getY(),rmap.getKeyLocations().getPools().get(0).getY());

        //COMPARING DOGPARKS
        Assert.assertEquals(map.getKeyLocations().getDogParks().size(),rmap.getKeyLocations().getDogParks().size());

        Assert.assertEquals(map.getKeyLocations().getDogParks().get(0).getX(),rmap.getKeyLocations().getDogParks().get(0).getX());
        Assert.assertEquals(map.getKeyLocations().getDogParks().get(0).getY(),rmap.getKeyLocations().getDogParks().get(0).getY());

        //COMPARING WORKPLACES
        Assert.assertEquals(map.getKeyLocations().getWorkplaces().size(),rmap.getKeyLocations().getWorkplaces().size());

        Assert.assertEquals(map.getKeyLocations().getWorkplaces().get(0).getX(),rmap.getKeyLocations().getWorkplaces().get(0).getX());
        Assert.assertEquals(map.getKeyLocations().getWorkplaces().get(0).getY(),rmap.getKeyLocations().getWorkplaces().get(0).getY());
    }




}